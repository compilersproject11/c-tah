(*structure Lex =
struct 
  structure CprsrLrVals = CprsrLrValsFun(structure Token = LrParser.Token)
  structure Lex = CprsrLexFun(structure Tokens = CprsrLrVals.Tokens)
  structure CprsrP = Join(structure ParserData = CprsrLrVals.ParserData
			structure Lex=Lex
			structure LrParser = LrParser)
  fun parse filename =
      let val file = TextIO.openIn filename
	  fun get _ = TextIO.input file
	  val lexer = Lex.makeLexer get
	  fun do_it() =
	      let val t = lexer()
	       in print t; print "\n";
		   if substring(t,0,3)="EOF" then () else do_it()
	      end
       in do_it();
	  TextIO.closeIn file
      end

end*)

structure Parse =
struct 
  structure CprsrLrVals = CprsrLrValsFun(structure Token = LrParser.Token)
  structure Lex = CprsrLexFun(structure Tokens = CprsrLrVals.Tokens)
  structure CprsrP = Join(structure ParserData = CprsrLrVals.ParserData
			structure Lex=Lex
			structure LrParser = LrParser)
  fun parse filename =
      let val _ = (ErrorMsg.reset(); ErrorMsg.fileName := filename)
	  val file = TextIO.openIn filename
	  fun get _ = TextIO.input file
	  fun parseerror(s,p1,p2) = ErrorMsg.error p1 s
	  val lexer = LrParser.Stream.streamify (Lex.makeLexer get)
	  val (absyn, _) = CprsrP.parse(30,lexer,parseerror,())
       in TextIO.closeIn file;
	   absyn
      end handle LrParser.ParseError => raise ErrorMsg.Error



end

