type pos = int
type svalue = Tokens.svalue
type ('a, 'b) token = ('a, 'b) Tokens.token
type lexresult = (svalue, pos) token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
fun err(p1,p2) = ErrorMsg.error p1
fun eof() = let val pos = hd(!linePos) in Tokens.EOF(pos,pos) end


%%
%header (functor CprsrLexFun(structure Tokens : Cprsr_TOKENS));






DIGIT=[0-9]+;
COMMENT=\/\/;
SPACE=[\ \t\b\f\r]+;
ID=[_a-zA-Z][_a-zA-Z0-9]*;
QUOTE = ["]; 
NOTQUOTE = [^"];

%%


"+"     => (Tokens.PLUS(yypos,yypos+1));
"-"     => (Tokens.MINUS(yypos,yypos+1));
"*"     => (Tokens.MUL(yypos,yypos+1));
"/"     => (Tokens.DIVIDE(yypos,yypos+1));

"=="    => (Tokens.EQ(yypos,yypos+1));

">"	=> (Tokens.GT(yypos,yypos+1));

"<"	=> (Tokens.LT(yypos,yypos+1));

";"     => (Tokens.SEMICOLON(yypos,yypos+1));
"="	=> (Tokens.ASSIGN(yypos,yypos+2));

"("     => (Tokens.LPAREN(yypos,yypos+1));
")"     => (Tokens.RPAREN(yypos,yypos+1));
\n    => (lineNum := !lineNum+1; continue() );

int   => (Tokens.INT(yypos,yypos+3));
else     => (Tokens.ELSE(yypos,yypos+4));
if       => (Tokens.IF(yypos,yypos+2));
endif	 => (Tokens.ENDIF(yypos,yypos+5));
endif	 => (Tokens.ENDIF(yypos,yypos+5));
show      => (Tokens.SHOW(yypos,yypos+4));
read      => (Tokens.READ(yypos,yypos+4));
endwhile	 => (Tokens.ENDWHILE(yypos,yypos+8));
{DIGIT} => (Tokens.DIGIT( valOf(Int.fromString yytext) , yypos , yypos + size yytext ));
{COMMENT}    => (continue());
{SPACE}      => (continue());
.            => (ErrorMsg.error yypos ("illegal character " ^ yytext); continue());
{ID} => (Tokens.ID(yytext,yypos,yypos+size yytext));
{QUOTE}{NOTQUOTE}*{QUOTE} => (Tokens.STRING(yytext, yypos, yypos + size yytext));

