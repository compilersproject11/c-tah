# C-Tah (Cheetah) Compiler

* This repository is for maintaining Compilers project.
* This project aims at writing compiler for a new C-Tah language and target it to C.
* Version 1.0

## Features

The compiler supports
* Loops
* Conditional Statement
* Integer datatype
* Read and show statements
* Expressions that are arithmetic operations on it.

## Running the tests

The following steps guide through the running of compiler.

* Clone the repository.
* Create a sample file in C-Tah language.
* Run the following commands
 ``` $ ml-lex regex.sml```
 ``` $ ml-yacc cgrammar.grm```
 ``` $ sml```
Make the sources.cm file by the instruction
``` - CM.make "sources.cm";```

Enter the name of file you want to compile when asked for.


Then your done. A file with the same name but with ".c" extension gets created.

## Results

| Code in C-tah  | Code generated      |
|----------------|---------------------|
| int cat;       | #include<stdio.h>   |
| int dog;       | void main()         |
| cat = 5;       | {                   |
| dog = 6;       | int cat;            |
| if(cat == dog) | int dog;            |
| dog = 7;       | cat =5;             |
| else           | dog =6;             |
| if(cat == 5)   | if (cat==dog)       |
| dog = 0;       | {                   |
| read (dog);    | dog=7;              |
| show("dog");   | }                   |
| show(dog);     | else                |
| endif;         | {                   |
| endif;         | if(cat==5)          |
|                | {                   |
|                | dog=0;              |
|                | }                   |
|                | scanf(" %d ",&dog); |
|                | printf( "dog");     |
|                | printf(" %d ",dog); |
|                | }                   |
|                | }                   |
|                | }                   |


## Built With

* smlnj-lib.cm  :: Compiler and programming environment for the Standard ML programming language
* basis.cm  :: To help in porting CM files
* sources :: File for ML-Yacc library


## Authors

* Akshat Choube    		+91-9497161221
* Jayaprakash Akula		+91-9497248284

## License

This project is licensed under the UNLINCENSE (LICENSE.md) file for details

