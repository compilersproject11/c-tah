signature CLANG =
sig
type linenum (* = int *)
type token

val FUNCTION:  linenum * linenum -> token
val BREAK:  linenum * linenum -> token
val FOR:  linenum * linenum -> token
val WHILE:  linenum * linenum -> token
val ELSE:  linenum * linenum -> token
val IF:  linenum * linenum -> token
val ENDIF:  linenum * linenum -> token
val ASSIGN:  linenum * linenum -> token



val GT:  linenum * linenum -> token

val LT:  linenum * linenum -> token

val EQ:  linenum * linenum -> token
val DIVIDE:  linenum * linenum -> token
val MUL:  linenum * linenum -> token
val MINUS:  linenum * linenum -> token
val PLUS:  linenum * linenum -> token
val DOT:  linenum * linenum -> token


val RBRACK:  linenum * linenum -> token
val LBRACK:  linenum * linenum -> token
val RPAREN:  linenum * linenum -> token
val LPAREN:  linenum * linenum -> token
val SEMICOLON:  linenum * linenum -> token
val COLON:  linenum * linenum -> token

val STRING: (string) *  linenum * linenum -> token
val INT: linenum * linenum -> token
val ID: (string) *  linenum * linenum -> token
val NEWLINE: linenum * linenum -> token
val EOF:  linenum * linenum -> token
end
