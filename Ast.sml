(* The abstract syntax tree for expression *)

structure Ast =
struct
datatype Relop = Equalto|GT|LT
datatype BinOp = Plus | Minus | Mul|Div;
datatype ID = Id of string 
datatype INT = Int
datatype Val =  Const of int
			| Idf of ID
datatype Equals = Eq
datatype STRING = St of string 
datatype Rhs =  Op    of Rhs * BinOp * Rhs |Cn of Val 
datatype Decl =  D of ID

datatype ModifyExpr = ME of ID*Rhs
datatype Expr  = R of Rhs| M of ModifyExpr
datatype Condexpr = 
	 CV of Val
	| Opc of Val*Relop*Val
datatype Stmt =  PS of Expr
			|   IFST of  Condexpr*Stmt list
			|   IFTE of Condexpr*Stmt list*Stmt list
			|   LOOP of Condexpr*Stmt list
			|   DS of Decl
			|   SH of ID
			|   SS of STRING
			|   RH of ID


(* The abstract syntax for expressions *)







(* Some helper functions *)
fun plus  a b = Op (a, Plus, b)
fun minus a b = Op (a, Minus, b)
fun divi   a b = Op (a, Div, b)
fun mul   a b = Op (a, Mul, b)
fun gt   a b = Opc(a, GT, b)
fun lt   a b = Opc (a, LT, b)
fun equalto   a b = Opc (a, Equalto, b)

fun decl   a  = D (a)
fun stm1  a  = PS(a)
fun stm2  a  b  = IFST (a,b)
fun stm3 a b c  = IFTE (a,b,c)
fun stm4 a b = LOOP(a,b)
fun stm5 a = DS(a)
fun me1  a b  = ME(a,b)

fun rhs a = R(a)
fun me a = M(a)
fun sh a = SH(a)
fun str a = SS(a)
fun read a = RH(a)
end


