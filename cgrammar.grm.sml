functor CprsrLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Cprsr_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
(* This is the preamble where you can have arbitrary  sml code. For us
it is empty *)


end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\016\000\014\000\031\000\000\000\
\\001\000\005\000\017\000\000\000\
\\001\000\005\000\022\000\000\000\
\\001\000\005\000\050\000\000\000\
\\001\000\005\000\051\000\000\000\
\\001\000\005\000\052\000\000\000\
\\001\000\005\000\061\000\000\000\
\\001\000\005\000\063\000\000\000\
\\001\000\005\000\065\000\000\000\
\\001\000\006\000\000\000\000\000\
\\001\000\009\000\060\000\020\000\059\000\000\000\
\\001\000\009\000\064\000\000\000\
\\001\000\010\000\042\000\000\000\
\\001\000\010\000\043\000\000\000\
\\001\000\010\000\044\000\000\000\
\\001\000\010\000\048\000\000\000\
\\001\000\010\000\049\000\000\000\
\\001\000\011\000\024\000\000\000\
\\001\000\011\000\025\000\000\000\
\\001\000\011\000\028\000\000\000\
\\001\000\011\000\029\000\000\000\
\\001\000\013\000\058\000\000\000\
\\001\000\014\000\026\000\000\000\
\\001\000\014\000\035\000\000\000\
\\001\000\014\000\037\000\022\000\036\000\000\000\
\\067\000\000\000\
\\068\000\000\000\
\\069\000\001\000\016\000\008\000\015\000\012\000\014\000\014\000\013\000\
\\021\000\012\000\023\000\011\000\024\000\010\000\000\000\
\\070\000\000\000\
\\071\000\000\000\
\\072\000\000\000\
\\073\000\000\000\
\\074\000\000\000\
\\075\000\000\000\
\\076\000\000\000\
\\077\000\000\000\
\\078\000\000\000\
\\079\000\000\000\
\\079\000\015\000\027\000\000\000\
\\080\000\002\000\021\000\003\000\020\000\004\000\019\000\019\000\018\000\000\000\
\\081\000\000\000\
\\082\000\002\000\021\000\003\000\020\000\004\000\019\000\019\000\018\000\000\000\
\\083\000\000\000\
\\084\000\004\000\019\000\019\000\018\000\000\000\
\\085\000\019\000\018\000\000\000\
\\086\000\000\000\
\\087\000\004\000\019\000\019\000\018\000\000\000\
\\088\000\016\000\047\000\017\000\046\000\018\000\045\000\000\000\
\\089\000\000\000\
\\090\000\000\000\
\\091\000\000\000\
\\092\000\000\000\
\"
val actionRowNumbers =
"\027\000\001\000\040\000\039\000\
\\042\000\002\000\025\000\027\000\
\\017\000\018\000\022\000\038\000\
\\019\000\020\000\036\000\032\000\
\\000\000\000\000\000\000\000\000\
\\028\000\026\000\023\000\024\000\
\\051\000\000\000\000\000\000\000\
\\045\000\037\000\044\000\046\000\
\\043\000\012\000\013\000\014\000\
\\041\000\047\000\015\000\016\000\
\\003\000\004\000\005\000\000\000\
\\000\000\000\000\027\000\027\000\
\\035\000\034\000\033\000\049\000\
\\050\000\048\000\021\000\010\000\
\\006\000\027\000\007\000\031\000\
\\011\000\029\000\008\000\030\000\
\\009\000"
val gotoT =
"\
\\001\000\007\000\002\000\006\000\003\000\064\000\004\000\005\000\
\\006\000\004\000\007\000\003\000\008\000\002\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\007\000\002\000\021\000\004\000\005\000\006\000\004\000\
\\007\000\003\000\008\000\002\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\004\000\007\000\028\000\000\000\
\\006\000\004\000\007\000\030\000\000\000\
\\006\000\004\000\007\000\031\000\000\000\
\\006\000\004\000\007\000\032\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\004\000\007\000\036\000\000\000\
\\005\000\038\000\006\000\037\000\000\000\
\\005\000\039\000\006\000\037\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\051\000\000\000\
\\006\000\052\000\000\000\
\\006\000\053\000\000\000\
\\001\000\007\000\002\000\054\000\004\000\005\000\006\000\004\000\
\\007\000\003\000\008\000\002\000\009\000\001\000\000\000\
\\001\000\007\000\002\000\055\000\004\000\005\000\006\000\004\000\
\\007\000\003\000\008\000\002\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\007\000\002\000\060\000\004\000\005\000\006\000\004\000\
\\007\000\003\000\008\000\002\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 65
val numrules = 26
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | STRING of  (string)
 | ID of  (string) | DIGIT of  (int) | DECL of  (Ast.Decl)
 | MODIFYEXPR of  (Ast.ModifyExpr) | RHS of  (Ast.Rhs)
 | VAL of  (Ast.Val) | CONDEXP of  (Ast.Condexpr) | EXP of  (Ast.Expr)
 | PROGRAM of  (Ast.Stmt list) | STMLIST of  (Ast.Stmt list)
 | STM of  (Ast.Stmt)
end
type svalue = MlyValue.svalue
type result = Ast.Stmt list
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
nil
val noShift = 
fn _ => false
val showTerminal =
fn (T 0) => "DIGIT"
  | (T 1) => "PLUS"
  | (T 2) => "MINUS"
  | (T 3) => "MUL"
  | (T 4) => "SEMICOLON"
  | (T 5) => "EOF"
  | (T 6) => "NEWLINE"
  | (T 7) => "IF"
  | (T 8) => "ENDIF"
  | (T 9) => "RPAREN"
  | (T 10) => "LPAREN"
  | (T 11) => "WHILE"
  | (T 12) => "ENDWHILE"
  | (T 13) => "ID"
  | (T 14) => "ASSIGN"
  | (T 15) => "GT"
  | (T 16) => "LT"
  | (T 17) => "EQ"
  | (T 18) => "DIVIDE"
  | (T 19) => "ELSE"
  | (T 20) => "INT"
  | (T 21) => "STRING"
  | (T 22) => "SHOW"
  | (T 23) => "READ"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 23) $$ (T 22) $$ (T 20) $$ (T 19) $$ (T 18) $$ (T 17) $$ (T 16)
 $$ (T 15) $$ (T 14) $$ (T 12) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8)
 $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3) $$ (T 2) $$ (T 1)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.STMLIST STMLIST, STMLIST1left, 
STMLIST1right)) :: rest671)) => let val  result = MlyValue.PROGRAM (
 STMLIST )
 in ( LrTable.NT 2, ( result, STMLIST1left, STMLIST1right), rest671)

end
|  ( 1, ( ( _, ( MlyValue.STMLIST STMLIST, _, STMLIST1right)) :: ( _, 
( MlyValue.STM STM, STM1left, _)) :: rest671)) => let val  result = 
MlyValue.STMLIST ( STM :: STMLIST )
 in ( LrTable.NT 1, ( result, STM1left, STMLIST1right), rest671)
end
|  ( 2, ( rest671)) => let val  result = MlyValue.STMLIST ( []  )
 in ( LrTable.NT 1, ( result, defaultPos, defaultPos), rest671)
end
|  ( 3, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.EXP EXP, 
EXP1left, _)) :: rest671)) => let val  result = MlyValue.STM (
Ast.stm1 EXP)
 in ( LrTable.NT 0, ( result, EXP1left, SEMICOLON1right), rest671)
end
|  ( 4, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.STMLIST STMLIST, _, _)) :: _ :: ( _, ( MlyValue.CONDEXP 
CONDEXP, _, _)) :: _ :: ( _, ( _, IF1left, _)) :: rest671)) => let
 val  result = MlyValue.STM (Ast.stm2 CONDEXP STMLIST)
 in ( LrTable.NT 0, ( result, IF1left, SEMICOLON1right), rest671)
end
|  ( 5, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.STMLIST STMLIST2, _, _)) :: _ :: ( _, ( MlyValue.STMLIST 
STMLIST1, _, _)) :: _ :: ( _, ( MlyValue.CONDEXP CONDEXP, _, _)) :: _
 :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = 
MlyValue.STM (Ast.stm3  CONDEXP STMLIST1  STMLIST2 )
 in ( LrTable.NT 0, ( result, IF1left, SEMICOLON1right), rest671)
end
|  ( 6, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.STMLIST STMLIST, _, _)) :: _ :: ( _, ( MlyValue.CONDEXP 
CONDEXP, _, _)) :: _ :: ( _, ( _, WHILE1left, _)) :: rest671)) => let
 val  result = MlyValue.STM (Ast.stm4 CONDEXP STMLIST)
 in ( LrTable.NT 0, ( result, WHILE1left, SEMICOLON1right), rest671)

end
|  ( 7, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.DECL DECL,
 DECL1left, _)) :: rest671)) => let val  result = MlyValue.STM (
Ast.stm5 DECL)
 in ( LrTable.NT 0, ( result, DECL1left, SEMICOLON1right), rest671)

end
|  ( 8, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( MlyValue.ID ID
, _, _)) :: _ :: ( _, ( _, SHOW1left, _)) :: rest671)) => let val  
result = MlyValue.STM (Ast.sh (Ast.Id (ID)))
 in ( LrTable.NT 0, ( result, SHOW1left, SEMICOLON1right), rest671)

end
|  ( 9, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.STRING STRING, _, _)) :: _ :: ( _, ( _, SHOW1left, _)) :: 
rest671)) => let val  result = MlyValue.STM (
Ast.str ( Ast.St (STRING)))
 in ( LrTable.NT 0, ( result, SHOW1left, SEMICOLON1right), rest671)

end
|  ( 10, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( MlyValue.ID 
ID, _, _)) :: _ :: ( _, ( _, READ1left, _)) :: rest671)) => let val  
result = MlyValue.STM (Ast.read (Ast.Id (ID)))
 in ( LrTable.NT 0, ( result, READ1left, SEMICOLON1right), rest671)

end
|  ( 11, ( ( _, ( MlyValue.DIGIT DIGIT, DIGIT1left, DIGIT1right)) :: 
rest671)) => let val  result = MlyValue.VAL (Ast.Const DIGIT)
 in ( LrTable.NT 5, ( result, DIGIT1left, DIGIT1right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.ID ID, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.VAL (Ast.Idf (Ast.Id (ID)))
 in ( LrTable.NT 5, ( result, ID1left, ID1right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.RHS RHS, RHS1left, RHS1right)) :: rest671))
 => let val  result = MlyValue.EXP (Ast.rhs RHS)
 in ( LrTable.NT 3, ( result, RHS1left, RHS1right), rest671)
end
|  ( 14, ( ( _, ( MlyValue.MODIFYEXPR MODIFYEXPR, MODIFYEXPR1left, 
MODIFYEXPR1right)) :: rest671)) => let val  result = MlyValue.EXP (
Ast.me MODIFYEXPR)
 in ( LrTable.NT 3, ( result, MODIFYEXPR1left, MODIFYEXPR1right), 
rest671)
end
|  ( 15, ( ( _, ( MlyValue.RHS RHS, _, RHS1right)) :: _ :: ( _, ( 
MlyValue.ID ID, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.MODIFYEXPR (Ast.me1 (Ast.Id (ID)) RHS)
 in ( LrTable.NT 7, ( result, ID1left, RHS1right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.VAL VAL, VAL1left, VAL1right)) :: rest671))
 => let val  result = MlyValue.RHS (Ast.Cn VAL)
 in ( LrTable.NT 6, ( result, VAL1left, VAL1right), rest671)
end
|  ( 17, ( ( _, ( MlyValue.RHS RHS2, _, RHS2right)) :: _ :: ( _, ( 
MlyValue.RHS RHS1, RHS1left, _)) :: rest671)) => let val  result = 
MlyValue.RHS ( Ast.plus  RHS1 RHS2 )
 in ( LrTable.NT 6, ( result, RHS1left, RHS2right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.RHS RHS2, _, RHS2right)) :: _ :: ( _, ( 
MlyValue.RHS RHS1, RHS1left, _)) :: rest671)) => let val  result = 
MlyValue.RHS ( Ast.mul  RHS1 RHS2)
 in ( LrTable.NT 6, ( result, RHS1left, RHS2right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.RHS RHS2, _, RHS2right)) :: _ :: ( _, ( 
MlyValue.RHS RHS1, RHS1left, _)) :: rest671)) => let val  result = 
MlyValue.RHS ( Ast.divi  RHS1 RHS2 )
 in ( LrTable.NT 6, ( result, RHS1left, RHS2right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.RHS RHS2, _, RHS2right)) :: _ :: ( _, ( 
MlyValue.RHS RHS1, RHS1left, _)) :: rest671)) => let val  result = 
MlyValue.RHS ( Ast.minus  RHS1 RHS2 )
 in ( LrTable.NT 6, ( result, RHS1left, RHS2right), rest671)
end
|  ( 21, ( ( _, ( MlyValue.VAL VAL, VAL1left, VAL1right)) :: rest671))
 => let val  result = MlyValue.CONDEXP (Ast.CV VAL)
 in ( LrTable.NT 4, ( result, VAL1left, VAL1right), rest671)
end
|  ( 22, ( ( _, ( MlyValue.VAL VAL2, _, VAL2right)) :: _ :: ( _, ( 
MlyValue.VAL VAL1, VAL1left, _)) :: rest671)) => let val  result = 
MlyValue.CONDEXP ( Ast.gt  VAL1 VAL2 )
 in ( LrTable.NT 4, ( result, VAL1left, VAL2right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.VAL VAL2, _, VAL2right)) :: _ :: ( _, ( 
MlyValue.VAL VAL1, VAL1left, _)) :: rest671)) => let val  result = 
MlyValue.CONDEXP ( Ast.equalto  VAL1 VAL2)
 in ( LrTable.NT 4, ( result, VAL1left, VAL2right), rest671)
end
|  ( 24, ( ( _, ( MlyValue.VAL VAL2, _, VAL2right)) :: _ :: ( _, ( 
MlyValue.VAL VAL1, VAL1left, _)) :: rest671)) => let val  result = 
MlyValue.CONDEXP ( Ast.lt  VAL1 VAL2 )
 in ( LrTable.NT 4, ( result, VAL1left, VAL2right), rest671)
end
|  ( 25, ( ( _, ( MlyValue.ID ID, _, ID1right)) :: ( _, ( _, INT1left,
 _)) :: rest671)) => let val  result = MlyValue.DECL (
Ast.decl (Ast.Id (ID)))
 in ( LrTable.NT 8, ( result, INT1left, ID1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.PROGRAM x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Cprsr_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun DIGIT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.DIGIT i,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun MUL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun NEWLINE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun ENDIF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun ENDWHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.ID i,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun INT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.STRING i,p1,p2))
fun SHOW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun READ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
end
end
