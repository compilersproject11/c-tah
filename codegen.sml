structure CG = 
struct

fun getVal (Ast.Idf(Ast.Id(ID))) = ID
	|getVal (Ast.Const (digit)) = (Int.toString digit)




fun getRhs (Ast.Op (a, Ast.Plus, b)) = getRhs a ^"+" ^ getRhs b
	|getRhs (Ast.Op (a, Ast.Minus, b)) = getRhs a ^"-" ^ getRhs b
	|getRhs (Ast.Op (a, Ast.Mul, b)) = getRhs a ^"*" ^ getRhs b
	|getRhs (Ast.Op (a, Ast.Div, b)) = getRhs a ^"/" ^ getRhs b
	|getRhs (Ast.Cn (value)) = getVal value

fun getMod (Ast.ME(Ast.Id(ID) ,RHS)) =  ID ^"=" ^ getRhs RHS

fun getExpr (Ast.R(Rhs)) = getRhs Rhs
	|getExpr (Ast.M(ModifyExpr)) = getMod ModifyExpr




fun getCondExpr (Ast.Opc (a, Ast.GT, b)) = getVal a ^">" ^ getVal b
	|getCondExpr (Ast.Opc (a, Ast.LT, b)) = getVal a ^"<" ^ getVal b
	|getCondExpr (Ast.Opc (a, Ast.Equalto, b)) = getVal a ^"==" ^ getVal b
	|getCondExpr (Ast.CV (Val)) =  getVal Val


fun getDecl (Ast.D (Ast.Id(ID))) = "int " ^ ID 


fun getStatement (Ast.PS(Expr)) =  getExpr Expr ^ ";\n"
	|getStatement (Ast.IFST (CondExpr,Stmtlist)) = "if(" ^ getCondExpr CondExpr ^ ")\n{\n" ^ getStatementlist Stmtlist ^"}\n"
	|getStatement (Ast.IFTE (CondExpr,Stmtlist1,Stmtlist2)) = "if(" ^ getCondExpr CondExpr ^ ") \n{\n" ^ getStatementlist Stmtlist1 ^"}\n" ^ "else \n{\n" ^ getStatementlist Stmtlist2 ^"}\n" 
	|getStatement (Ast.LOOP(CondExpr,Stmtlist)) = "while(" ^ getCondExpr CondExpr ^ ") \n{\n" ^ getStatementlist Stmtlist ^"}\n"
	|getStatement (Ast.DS(decl)) = getDecl decl ^ ";\n"
	|getStatement (Ast.SH(Ast.Id(ID))) = ("printf(\" %d \"," ^ ID ^ ");\n")
	|getStatement (Ast.SS(Ast.St(STRING))) = ("printf( "^ STRING ^ ");\n")
	|getStatement (Ast.RH(Ast.Id(ID))) = ("scanf(\" %d \",&" ^ ID ^ ");\n")

and
	getStatementlist [] = ""
	| getStatementlist (x::xs) = getStatement x ^ getStatementlist xs


val _ =print(" enter filename \n");
val k = TextIO.inputLine TextIO.stdIn;
val filename=substring(valOf k ,0, (size (valOf(k))-1))
val outputname=substring(valOf k ,0, (size (valOf(k))-6)) ^".c"
val result = Parse.parse filename
val filecontent = getStatementlist result
fun writeFile filename content =
    let val fd = TextIO.openOut filename
        val _ = TextIO.output (fd, content) handle e => (TextIO.closeOut fd; raise e)
        val _ = TextIO.closeOut fd
    in () end

val filecontent = "#include<stdio.h>\nvoid main()\n{\n" ^ filecontent ^ "\n}" 
val _ = writeFile outputname filecontent 

end



